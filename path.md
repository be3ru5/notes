# Road Map

### Requirements

- Basic Programing

- Basic Computer Networking

### Theory

- Note taking and Report writing

- Linux Environment

- Linux Command Line

- Bash Scripting

- Powershell and Powershell Scripting

- File Transfers and Services

- Tools

- Passive Information Gathering

- Active Information Gathering

- Port Redirecting and Tunneling

### Practicals

- Vulnerability Scanning and Enumeration

- System Exploitation

- Privilege Escalation

- Exploit Development _(BoF)_

- Web Application Testing _(OWASP TOP 10)_

- Wireless Attacks

- Anti-virus Evasion/Firewall Bypass

- Active Directory Exploitation
