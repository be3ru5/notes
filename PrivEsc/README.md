# Privilege Escalation 

## Linux:

> source: [here](https://www.udemy.com/course/linux-privilege-escalation/)

- notes: [here](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/LinuxNotes.md)

- techniques: [here](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/LinuxPrivEsc.md)

## Windows:

> source: [here](https://www.udemy.com/course/windows-privilege-escalation/)

- notes: [here](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/WindowsNotes.md)

- techniques: [here](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/WinPrivEsc.md)
