# Linux Privilege Escalation Notes
> 15.07.2021 | [techniques](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/LinuxPrivEsc.md)
______

### Permissions in Linux:

USERS:

1. *user accounts* are configured in the `/etc/passwd` file.
2. *user password* hashes are stored in the `/etc/shadow` file.
3. user has user ID (UID). **root**'s UID is 0.

GROUPS:

1. *groups* are configured in the `/etc/group` file.
2. users have a primary group and can have multiple secondary/supplementary groups. *primary group* has the same name as their user account.

FILES & DIRECTORIES:

1. permissions are defined in terms of read, writ and execute operation.
2. there are 3 sets of permission -> user-group-other_users(world)
3. only owner can change the permissions
4. directory permission:
    * execute: the directory can be entered. Without this permission, neither the read nor write permissions will work.
    * read: the directory contents can be listed.
    * write: files and subdirectories can be created in the directory.

SPECIAL PERMISSIONS:

1. setuid (suid) bit: files will get executed with the privileges of the file owner.
2. setgid (sgid) bit: the file will get executed with the privileges of the file group.

PERMISSIONS:

![image](./imgs/perms.png)

The first 10 characters indicate the permissions set on the file
or directory.

* first character; files:`-` directory:`d`
* the remaining 9 characters represent the 3 sets of permissions (owner, group, others) as read `r`, write `w`, execute `e` and SUID/SGID `s`
* Real, Effective, & Saved are types of UID/GID

### TOOLS:

1. [Linux Smart Enumeration](https://github.com/diego-treitos/linux-smart-enumeration/blob/master/lse.sh)
2. [LinEnum](https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh)
3. [Linux Exploit Suggester 2](https://raw.githubusercontent.com/jondonas/linux-exploit-suggester-2/master/linux-exploit-suggester-2.pl)
