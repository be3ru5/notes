# Windows Privilege Escalation Notes
> 20.07.2021 | [techniques](https://gitlab.com/be3ru5/notes/-/blob/master/PrivEsc/WinPrivEsc.md)
_______

## overview:

In a lot of cases, privilege escalation may not simply rely on a single misconfiguration, but may require you to think, and combine multiple misconfigurations.

Service accounts cannot be used to sign into a Windows system.

The SYSTEM account is a default service account which has the highest privileges of any local account in Windows.

User accounts can belong to multiple groups, and groups can have multiple users.

In Windows, there are multiple types of resource (also known as objects):

- files / directories
- registry entries
- services

Permissions to access a certain resource in Windows are controlled by the Access Control List (ACL) for that resource.

Each ACL is made up of zero or more access control entries (ACEs).

Each ACE defines the relationship between a principal (e.g. a user, group) and a certain access right.

## exploitation:

1. kernel exploits
2. service exploits
	* insecure service properties *(DACL service)*
	* unquoted service path
	* weak registry permission
	* insecure service executable
	* DLL hijacking
3. registry exploits
	* autoruns
	* AlwaysInstallElevated
4. passwords
	* registry
	* saved credentials
	* configuration files
	* SAM/SYSTEM Locations
	* passing the hash
5. scheduled tasks
6. insecure GUI apps
7. startup apps

**Popular PrivEsc Exploits:**

- hot potato
- juicy potato

## tools:

- finding kernel exploits: [wes.py](https://github.com/bitsadmin/wesng/blob/master/wes.py)

- to find misconfiguration: [winpeas.exe](https://github.com/carlospolop/PEASS-ng/tree/master/winPEAS/winPEASexe)

- to check permissions: [accesschk.exe](https://web.archive.org/web/20111111130246/http://live.sysinternals.com/accesschk.exe)

- to monitor running process: [procmon.exe](https://download.sysinternals.com/files/ProcessMonitor.zip)

## Strategy

- Run Seatbelt & other scripts as well!

- Try things that don’t have many steps first, e.g. registry exploits, services

- Have a good look at admin processes, enumerate their versions and search for exploits.

- If you still don’t have an admin shell, re-read your full enumeration dumps and highlight anything that seems odd. This might be a process or file name you aren’t familiar with or even a username. At this stage you can also start to think about Kernel Exploits.
