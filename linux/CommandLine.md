# Linux Command Line Functions

## Bash environment variable:

### To check variable value

`env`

To display all environment variables

`echo $VARIABLE`

```
┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games:/home/kali/.local/bin
```

### To create new veriable

`VARIABLENAME=variablevalue`

```
┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ LALIT=8055297090

┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ echo $LALIT
8055297090
```

### To remove variable name

`unset VARIABLENAME`

```
┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ unset LALIT

┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ echo $LALIT

```

> Didn't return any result

### To set new value to existing variable

`export VARIABLENAME=VARIABLEVALUE`

```
┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ echo $IP
10.0.2.15

┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ export IP=192.168.1.11

┌──(kali㉿kali)-[~/STORAGE/temp]
└─$ echo $IP
192.168.1.11
```

## Redirection

|   File    |   File Descriptor |
| --- | --- |
| Standard Input STDIN | FD0 |
| Standard Output STDOUT | FD1 |
| Standard Error STDERR | FD2 |

**Redirecting to a New file**

`>` is the output redirection operator.

**Redirecting to an Existing file**

`>>` appends output to an existing file.

**Redirecting from a File**

`<` is the input redirection operator.

`>&` redirects output of one file to another

**Redirecting STDERR**

`COMMANDS 2> errorfile`

## Text Searching and Manipulation

### pipe ` | `

To run two commands consecutively

### grep

`grep <SEARCHI_STRING>`

|   Options	|	Function	|
| --- | --- |
| -v | Show all the files that do not match the search string |
| -c | print only a count of selected lines per files |
| -n | print line number with output lines |
| -i | Match both (upper and lower) case |
| -l | print only names of FILEs with selected lines |

> `grep --help` for more

## Managing Processes

### fg and bg

**`bg`:** run process from terminal then hit 'ctrl + Z' to stop the process then run `gb`. So that it will shift process to backgroud from foreground

**`fg`:** to bring background command back to foreground. `fg <PROCESSNAME>`

### ps

`ps ux`:

Lists all the processes running under the current user.

`ps <PID>`:

To show process status of a single command.

> for **PID** `pidof <PROCESSNAME>`

> `top` to check all running processes

### kill

To kill the process

`kill <PID>`

### watch

loops the following command until user kills using 'ctrl+C'

### alias

`alias NEWCOMMAND=EXISTINGCOMMAND`

Creates new substitutionary command for given existing command

> `unalias NEWCOMMAND` to remove new command
