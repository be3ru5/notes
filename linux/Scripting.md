# Shell Scripting

### Variables

```
NAME="Lalit"	# assigned
SPORT="Foot"	# assigned
echo "Most popular sport is ${SPORT}ball, according to $NAME"	# calling
```

OP:
> Most popular sport is Football, according to Lalit

### Input & Output

```
read -p "Enter something: " PROMPT_1 PROMPT_2
echo "Name is: $PROMPT_1"	# OP 1
echo "Here is: $PROMPT_2"	# OP 2
```

OP:
>Enter something: Hey There is you ...

>Here is: Hey

>Here is: There is you ...

### if & if/else

```
read -p "Enter name: " NAME
# spaces are important
if [ "$NAME" = "lalit" ];
then
	echo "permission granted"
else
	echo "permission denied"
fi	# to exit from loop
```

OP1:
>Enter name: lalit

>permission granted

OP2:
>Enter name: rca

>permission denied

### Test Scripts

### for loop

```
for CHARACTORS in $(cat names.txt); do
	echo "Char is: $CHARACTORS"
done
```

OP:
>Char is: Zoro

>Char is: Robin

### Functions

```
function test_shadow(){	# 1 way declaring function
	if [ -e /etc/shadow ]; then
		echo "file available"
	else
		echo "negative"
	fi
	test_passwd	# calling function in function
}
test_passwd(){	# 2 way declaring function 
if [ -e /etc/lalit ];
then
	echo "file available"
else
	echo "negative"
fi
}
test_shadow	# calling functions
```

OP:
>file available

>negative

### positional parameters

parameter counts: 0 - 9.

0 is set of script file. for manual use available parameters are 1-9.

```
echo "parameter 0:$0"
echo "parameter 1:$1"
echo "parameter 2:$2"
echo "parameter 3:$3"
echo "parameter 4:$4"
```

OP:
>parameter 0:./test.sh

>parameter 1:lalit

>parameter 2:21

>parameter 3:good boy

>parameter 4:
