**/bin**

binary files.

**/sbin**

binary files for single user.

**/boot**

boot loader files.

**/cdrom**

legacy mounting folder.

**/dev**

hardware drivers/application (HDD,SSD,Webcam,etc.).

**/etc**

'edit to configure'

all system configuration files are stored over here.

**/lib /lib32 /lib64**

these are library files of applications to function.

**/mnt /media**

other mounted disks. eg: usb,external drive,secondary storage drive.

> mnt to mount manually

> media to auto mount by OS

**/opt**

to install software manually (to vendors).

**/proc**

where sudo files are stored.

**/root**

root user's home folder.

**/run**

tempFS files system which runs in RAM. To store run time information. Will be empty on system reboot.

**/snap**

folder used by Ubuntu based systems. To store snap packages.

**/srv**

Service directory. like FTP,HTTP for external user to access it.

**/sys**

System directory. Interact with kernel, change setting in graphic card. Runs in RAM

**/tmp**

Application temporarily stores files for sessions. Runs on RAM.

**/usr**

user application space where applications installed that are used by user.

1. source code installed programs in `local`
2. larger programs in `share`
3. installed source code like kernel source and header files in `src`

**/var**

variable directory. Contains files and directories which contains that are expected to grow in size. Example:

1. `log` contains log files
2. `crash` contains information about system/application crash
3. databases
4. backups

**/home**

where individual user stores personal files and documents

**hidden files in Linux**

Hidden files/directory in Linux/UNIX starts with "." (period).

**Finding lifes in Linux**

`find`

[cheatsheet](https://devhints.io/find)

`locate`

`whereis`

